run:
	docker-compose up

bash:
	docker-compose exec web bash

install:
	docker-compose exec web install-magento

sampledata:
	docker-compose exec web install-sampledata

dev_mode:
	docker-compose exec web rm -rf generated/metadata/* generated/code/*
	docker-compose exec web bin/magento deploy:mode:set developer

fix_perm:
	docker-compose exec web chmod -R 777 generated/ var/cache/ var/log/

di_compile:
	docker-compose exec web rm -rf generated/metadata/* generated/code/*
	docker-compose exec web bin/magento setup:di:compile