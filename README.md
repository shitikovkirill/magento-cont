# Magento cont

## Run
```bash
docker-compose up -d

WEB_CONTAINER=$(docker ps --format {{.Names}} | grep web)

docker exec -it "$WEB_CONTAINER" install-magento

docker exec -it "$WEB_CONTAINER" install-sampledata

docker-compose exec web bash
```